require 'rspec'
require 'calc'

describe 'Calculator' do

  calc = Calculator.new

  it 'takes an empty string and returns a 0' do

    expect(calc.add("")).to eq(0)

  end

  it 'takes a single number as a string and returns that number' do

    @RandomNumber = Random.new.rand(1..100).to_s

    expect(calc.add(@RandomNumber)).to eq(@RandomNumber.to_i)

  end

  it 'takes two numbers as a string, separated by a comma, and returns their summation' do

    @RandomNumber1 = Random.new.rand(1..100)
    @RandomNumber2 = Random.new.rand(1..100)

    puts "Random Number 1: " + @RandomNumber1.to_s
    puts "Random Number 2: " +@RandomNumber2.to_s

    @RandomNumberString = @RandomNumber1.to_s + "," + @RandomNumber2.to_s

    expect(calc.add(@RandomNumberString)).to eq(@RandomNumber1 + @RandomNumber2)

  end

end